// Copyright (c) 2015 kay All rights reserved.
package kiang.teb;

import java.util.Date;

/**
 * @author kay
 */
public abstract class TebModelBase implements TebModel {
    private int code;
    private Date date;
    private boolean bool;

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean isBool() {
        return bool;
    }

    @Override
    public void setBool(boolean bool) {
        this.bool = bool;
    }
}
