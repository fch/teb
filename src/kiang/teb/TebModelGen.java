// Copyright (c) 2015 kay All rights reserved.
package kiang.teb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author kay
 */
public final class TebModelGen {

    public static List<TebModel> dummyModels(int count) {
        long step = 60 * 1000;
        long time = new Date().getTime() - count * step;
        final List<TebModel> models = new ArrayList<TebModel>(count);
        TebModel model;
        for (int i = 0; i < count; i++) {
            model = (i & 1) == 0 ? new TebModelMin() : new TebModelMax();
            model.setCode(1000 + i);
            model.setName("\u6D4B\u8BD5\u6A21\u578B-" + i);
            model.setDate(new Date(time + i * step));
            model.setBool((i & 1) == 0);
            model.setValue(i * 1.3d);
            models.add(model);
        }
        return models;
    }
}
