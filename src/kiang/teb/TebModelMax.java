// Copyright (c) 2015 kay All rights reserved.
package kiang.teb;

/**
 * @author kay
 */
public final class TebModelMax extends TebModelBase {
    private String name;
    private double value;

    @Override
    public String getName() {
        return "Max-" + name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return 200d + value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
