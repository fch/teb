package kiang.teb;

import java.util.Date;

/**
 * Copyright (c) 2014 by kiang
 *
 * @author kiang
 * @version 0.1-pre
 */
public interface TebModel {

    public int getCode();

    public void setCode(int code);

    public String getName();

    public void setName(String name);

    public Date getDate();

    public void setDate(Date date);

    public boolean isBool();

    public void setBool(boolean bool);

    public double getValue();

    public void setValue(double value);
}
