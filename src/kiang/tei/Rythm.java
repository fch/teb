package kiang.tei;

import kiang.teb.TebEngine;
import kiang.teb.TebModelGen;
import org.rythmengine.RythmEngine;

import java.io.OutputStream;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Copyright (c) 2014 by kiang
 *
 * @author kiang
 * @version 0.1-pre
 */
public final class Rythm implements TebEngine {
    private static ThreadLocal<SimpleDateFormat> DFL = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };
    private RythmEngine engine;

    @Override
    public TebEngine init(Properties properties) throws Exception {
        final Properties ps = new Properties();
        ps.put("log.enabled", false);
        ps.put("feature.smart_escape.enabled", false);
        ps.put("feature.transform.enabled", false);
        ps.put("home.template", Rythm.class.getResource("/kiang/tpl").toURI().toURL().getFile());
        //ps.put("codegen.dynamic_exp.enabled", true);
        //ps.put("built_in.code_type", "false");
        //ps.put("built_in.transformer", "false");
        //ps.put("engine.file_write", "false");
        //ps.put("codegen.compact.enabled", "false");
        //ps.put("home.tmp", "c:\\tmp");
        //ps.put("engine.mode", Rythm.Mode.dev);
        engine = new RythmEngine(ps);
        return this;
    }

    @Override
    public void test(Map arguments, Writer writer) throws Exception {
        engine.render(writer, "rythm.tpl", arguments.get("source"), arguments.get("format"), arguments.get("models"));
    }

    @Override
    public void test(Map arguments, OutputStream output) throws Exception {
        engine.render(output, "rythm.tpl", arguments.get("source"), arguments.get("format"), arguments.get("models"));
    }

    @Override
    public boolean isBinarySupport() {
        return true;
    }

    @Override
    public void shut() throws Exception {
        engine.shutdown();
    }

    public static void main(String args[]) throws Exception {
        String source="UTF-8", target = "UTF-8";
        OutputStream output = System.out;
        Map data = new HashMap();
        data.put("target", target);
        data.put("format", true);
        data.put("models", TebModelGen.dummyModels(20));
        Properties properties = new Properties();
        properties.setProperty("source", source);
        properties.setProperty("target", target);
        properties.setProperty("binary", String.valueOf(true));
        TebEngine engine = new Rythm().init(properties);
        engine.test(data, output);
        output.flush();
        engine.shut();
    }
}
