package kiang.tei;


import kay.wet.Engine;
import kiang.teb.TebEngine;
import kiang.teb.TebModelGen;

import java.io.OutputStream;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Copyright (c) 2014 by kiang
 *
 * @author kiang
 * @version 0.1-pre
 */
public final class Wet implements TebEngine {
    private Engine engine;

    @Override
    public TebEngine init(Properties properties) throws Exception {
        final Properties ps = new Properties();
        ps.setProperty("wet.input.encode", properties.getProperty("source", "UTF-8"));
        ps.setProperty("wet.output.encode", properties.getProperty("target", "UTF-8"));
        ps.setProperty("wet.output.binary", properties.getProperty("binary", "true"));
        ps.setProperty("wet.template.cache", "true");
        ps.setProperty("wet.reflect.factory", "kay.wet.reflect.RxAsmFactory");
        ps.setProperty("wet.loader.class", "kay.wet.loader.ClassPathLoader");
        ps.setProperty("wet.loader.space", "");
        ps.setProperty("wet.security", "kay.wet.security.DefaultSecurity");
        ps.setProperty("wet.textFilter", "");
        ps.setProperty("wet.debugger", "");
        engine = Engine.get(ps);
        return this;
    }

    @Override
    public void test(Map arguments, Writer writer) throws Exception {
        engine.getTemplate("kiang/tpl/wet.tpl").execute(arguments, writer);
    }

    @Override
    public void test(Map arguments, OutputStream output) throws Exception {
        engine.getTemplate("kiang/tpl/wet.tpl").execute(arguments, output);
    }

    @Override
    public boolean isBinarySupport() {
        return true;
    }

    @Override
    public void shut() throws Exception {
    }

    public static void main(String args[]) throws Exception {
        String source="UTF-8", target = "UTF-8";
        OutputStream output = System.out;
        Map data = new HashMap();
        data.put("target", target);
        data.put("format", true);
        data.put("models", TebModelGen.dummyModels(20));
        Properties properties = new Properties();
        properties.setProperty("source", source);
        properties.setProperty("target", target);
        properties.setProperty("binary", String.valueOf(true));
        TebEngine engine = new Wet().init(properties);
        engine.test(data, output);
        output.flush();
        engine.shut();
    }
}
